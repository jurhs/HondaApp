import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
///////////////////////////////////////////////////////////
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {NoopAnimationsModule} from '@angular/platform-browser/animations';
import {MatFormFieldModule} from '@angular/material/form-field';
import { AppComponent } from './app.component';
import {MatInputModule, MatCalendar, MatNativeDateModule} from '@angular/material';
import {MatIconModule} from '@angular/material/icon';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {MatButtonModule} from '@angular/material/button';
import {MatToolbarModule} from '@angular/material/toolbar';
import {MatMenuModule} from '@angular/material/menu';
import {MatDatepickerModule} from '@angular/material/datepicker';
import {MatCardModule} from '@angular/material/card';
//////////////////////////////////////////////////////////////////////
import {CalendarComponent} from 'ap-angular2-fullcalendar/src/calendar/calendar';
import { app_routing } from './app.routing';
import { NavbarComponent } from './components/navbar/navbar.component';
import { RegistrarComponent } from './components/registrar/registrar.component';
import { InicioComponent } from './components/inicio/inicio.component';
import { LoginComponent } from './components/login/login.component';
import { FechaComponent } from './components/fecha/fecha.component';
import { PerfilComponent } from './components/perfil/perfil.component';
import { HoraComponent } from './components/hora/hora.component';
import { LocalesComponent } from './components/locales/locales.component';
import { SidenavComponent } from './components/sidenav/sidenav.component';

@NgModule({
  declarations: [
    AppComponent,
    RegistrarComponent,
    NavbarComponent,
    InicioComponent,
    LoginComponent,
    FechaComponent,
    CalendarComponent,
    HoraComponent,
    PerfilComponent,
    LocalesComponent,
    SidenavComponent
  ],
  imports: [
    BrowserModule,
    app_routing,
    BrowserAnimationsModule,
    NoopAnimationsModule,
    MatFormFieldModule,
    MatInputModule,
    MatIconModule,
    FormsModule,
    ReactiveFormsModule,
    MatButtonModule,
    MatToolbarModule,
    MatDatepickerModule,
    MatNativeDateModule,
    MatCardModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
