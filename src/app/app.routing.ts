import { RouterModule, Routes } from '@angular/router';
import { ModuleWithProviders } from '@angular/core';
import { LoginComponent } from './components/login/login.component';
import { RegistrarComponent } from './components/registrar/registrar.component';
import { NavbarComponent } from './components/navbar/navbar.component';
import { FechaComponent } from './components/fecha/fecha.component';
import { InicioComponent } from './components/inicio/inicio.component';
import { HoraComponent } from './components/hora/hora.component';
import { PerfilComponent } from './components/perfil/perfil.component';

const APP_ROUTES: Routes = [
    { path: 'login', component: LoginComponent },
    { path: 'registrar', component: RegistrarComponent },
    { path: 'navbar', component: NavbarComponent },
    { path: 'inicio', component: InicioComponent },
    { path: 'fecha', component: FechaComponent },
    { path: 'hora', component: HoraComponent },
    { path: 'perfil', component: PerfilComponent },
    { path: '**', pathMatch: 'full', redirectTo: 'inicio'}
];

export const app_routing =  RouterModule.forRoot(APP_ROUTES);
