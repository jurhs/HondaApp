import { Component, OnInit } from '@angular/core';
import { trigger, state, style, transition, animate, keyframes } from '@angular/animations';

@Component({
  selector: 'app-sidenav',
  templateUrl: './sidenav.component.html',
  styleUrls: ['./sidenav.component.css'],
  animations: [
    trigger('closeSideBlock', [
        state('small', style([{
            backgroundColor: 'rgba(0, 0, 0, 0.700)'},
        ])),
        state('large', style([{
          backgroundColor: 'rgba(0, 0, 0, 0)'}, { visibility: 'hidden'}
      ])),
        transition('small <=> large', animate('1s ease-in', style({
            transform: 'translateX(-125vw)'
        }))),
    ]),
    trigger('closeSide', [
      state('small', style({
        size: '',
      })),
      state('large', style({
        height: '50vw', width: '50vw',
      })),
      transition('small <=> large', animate('1s ease-in', style({
        height: '50vw', width: '50vw',
      }))),
  ]),
  ]
})
export class SidenavComponent implements OnInit {
  sideStatus: string;
  sidenavBlockCont: string;
  state = 'small';

  constructor() { }

  ngOnInit() {
      this.sideStatus = 'false';
      this.sidenavBlockCont = 'false';
  }

  close() {
    this.state = (this.state === 'small' ? 'large' : 'small');
  }

}
