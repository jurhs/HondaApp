import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-locales',
  templateUrl: './locales.component.html',
  styleUrls: ['./locales.component.css']
})
export class LocalesComponent implements OnInit {
  centros = [
    {
      centro: 'Pradera Zona 10',
      ubicacion: '18 calle 22-59 zona 10'
    },
    {
      centro: 'Condado Concepción',
      ubicacion: 'km. 15.5 Carretera a El Salvador, Condado Concepción'
    },
    {
      centro: 'Pradera Concepción',
      ubicacion: 'Km. 15.8 Carretera al Salvador C.C. Pradera Concepción, sótano 2.'
    },
    {
      centro: 'Zona 9',
      ubicacion: '7a. Avenida 14-78 zona 9'
    },
    {
      centro: 'Majadas',
      ubicacion: '30 Ave. 6-50 zona 11'
    },
    {
      centro: 'Centro de Enderezado y Pintura',
      ubicacion: '10 Ave. 26-30 Zona 13'
    },
  ];
  centroSeleccionado: any;

  constructor() { }

  ngOnInit() {
  }

  seleccionar(centro: any) {
    this.centroSeleccionado = centro;
    for (const elemento of this.centros) {
      document.getElementById(elemento.centro).classList.remove('active');
    }
    document.getElementById(this.centroSeleccionado.centro).classList.add('active');
    console.log(this.centroSeleccionado);
  }
}
