import { Component, OnInit, ViewEncapsulation } from '@angular/core';

@Component({
  selector: 'app-perfil',
  templateUrl: './perfil.component.html',
  styleUrls: ['./perfil.component.css'],
  encapsulation: ViewEncapsulation.None
})
export class PerfilComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

}
