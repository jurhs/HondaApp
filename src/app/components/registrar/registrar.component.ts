import {MatFormFieldModule} from '@angular/material/form-field';
import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { FormControl, Validators } from '@angular/forms';

@Component({
  selector: 'app-registrar',
  templateUrl: './registrar.component.html',
  styleUrls: ['./registrar.component.css'],
  encapsulation: ViewEncapsulation.None
})
export class RegistrarComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }
  // tslint:disable-next-line:member-ordering
  email = new FormControl('', [Validators.required, Validators.email]);
    getErrorMessage() {
      return this.email.hasError('required') ? 'Es un campo requerido' :
          this.email.hasError('email') ? 'email no valido ' :
              '';
    }

}
