import { Component, OnInit, ViewEncapsulation, ViewChild } from '@angular/core';
import { CalendarComponent } from 'ap-angular2-fullcalendar/src/calendar/calendar';

@Component({
  selector: 'app-fecha',
  templateUrl: './fecha.component.html',
  styleUrls: ['./fecha.component.css'],
  encapsulation: ViewEncapsulation.None
})
export class FechaComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

  // tslint:disable-next-line:member-ordering
  calendarOptions: Object = {
    height: 'parent',
    fixedWeekCount: false,
    defaultDate: '2017-11-15',
    editable: true,
    eventLimit: true,
    events: [
      {
        title: 'All Day Event',
        start: '2017-11-15'
      },
      {
        title: 'Long Event',
        start: '2017-11-15',
        end: '2017-11-16'
      }
    ]
  };

  // tslint:disable-next-line:member-ordering
  @ViewChild(CalendarComponent) myCalendar: CalendarComponent;
   changeCalendarView(view) {
     this.myCalendar.fullCalendar('changeView', view);

   }
}
